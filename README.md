# TDD EXAMPLES (Test Driven Development)

## Installation

Requires [RVM](https://rvm.io/) vto run.

Install ruby 3.0.0 using rvm
```sh
rvm install ruby-3.0.0
rvm use ruby-3.0.0
```

The current ruby version should be 3.0.0
```sh
ruby -v
ruby 3.0.0p0 (2020-12-25 revision 95aff21468) [x86_64-darwin20]
```

Install bundler
```sh
gem install bundler
bundle install
```

Run rspec
```sh
bundle exec rspec

Finished in 0.00416 seconds (files took 0.12497 seconds to load)
1 example, 0 failures
```

Run guard
```sh
bundle exec guard

10:06:41 - INFO - Guard::RSpec is running
10:06:41 - INFO - Guard is now watching at '/Users/[YourUserDirectory]/tdd'
[1] guard(main)>
```
